class opc_ntp::install {
    package { 'ntp':
        source     => 'file:///etc/puppetlabs/code/environments/production/modules/workspace/puppet/modules/ntp/manifests/ntp-4.2.6p5-22.el7.centos.1.x86_64.rpm',
          provider => 'rpm',
          ensure => installed,
    }

}
