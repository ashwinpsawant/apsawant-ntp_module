class opc_ntp::params {
  $ntp_servers    = hiera_array('ntp_server')
   $ntp_path      = '/etc/ntp.conf'
   $ntp_drift      = '/var/lib/ntp/drift'
   $ntp_template = 'ntp/ntp.conf.erb'
   $ntp_drift_template = 'ntp/drift.erb'
    notify { "Using ${ntp_servers}":}
}
