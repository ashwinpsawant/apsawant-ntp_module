class opc_ntp (
$ntp_servers  = $opc_ntp::params::ntp_servers,
$ntp_path     = $opc_ntp::params::ntp_path,
$ntp_drift    = $opc_ntp::params::ntp_drift,
$ntp_template = $opc_ntp::params::ntp_template,
$ntp_drift_template    = $ntp::params::ntp_drift_template,

) inherits  opc_ntp::params {
  validate_array($ntp_servers)
    validate_absolute_path($ntp_path)
    validate_absolute_path($ntp_drift)
    validate_string($ntp_template)
    validate_string($ntp_drift_template)

    class { '::opc_ntp::config': } ->
    class { '::opc_ntp::install': } ->
    Class['::opc_ntp']
}
