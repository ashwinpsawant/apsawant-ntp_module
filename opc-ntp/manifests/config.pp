class opc_ntp::config {
    include opc_ntp
    $ntp_servers    = $opc_ntp::ntp_servers
    $ntp_path     = $opc_ntp::ntp_path
    $ntp_drift  = $opc_ntp::ntp_drift
  file { $ntp_path:
         ensure   => present,
         backup   => true,
         owner    => 'root',
         group    => 'root',
         mode     => '0644',
         content  => template($ntp_template),
  }
  file { $ntp_drift:
         ensure   => present,
         backup   => true,
         owner    => 'ntp',
         group    => 'ntp',
         mode     => '0644',
         content  => template($ntp_drift_template),
  }
}
